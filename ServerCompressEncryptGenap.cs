//SERVER SIDE

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.IO.Compression;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;



namespace FileTransferServer
{

    class ServerCompressEncryptGenap
    {
        //AES DECRYPT
        public static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        //METHOD TO DECRYPT FILE
        public static void DecryptFile(string fileEncrypted, string file, string password)
        {
           
            byte[] bytesToBeDecrypted = File.ReadAllBytes(fileEncrypted);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            File.WriteAllBytes(file, bytesDecrypted);
        }

        //MAIN FUNCTION
        static void Main(string[] args)
        {
            //INSTANTIATE TCP LISTENER
            TcpListener tcpListener = new TcpListener(IPAddress.Any, 8080);
            tcpListener.Start();

            Console.WriteLine("Server started");
            bool connect = true;
            
            //WHILE CONNECTED WITH CLIENT
            while (connect)
            {
                //ACCEPTING TCP CONNECTION
                TcpClient tcpClient = tcpListener.AcceptTcpClient();

                Console.WriteLine("Connected to client");
                
                //INSTANTIATE NEW STREAMREADER
                StreamReader reader = new StreamReader(tcpClient.GetStream());
                StreamWriter writer = new StreamWriter(tcpClient.GetStream());

                //DECLARE VARIABLE
                string magicWord = reader.ReadLine(); 
                string cmdFileSize = reader.ReadLine();
                
                string cmdFileName = reader.ReadLine();
                string extractPath = cmdFileName;

                int bytesLength = Convert.ToInt32(cmdFileSize);
                byte[] buffer = new byte[bytesLength];
                int bytesReceived = 0;
                int bytesRead = 0;
                int size = 1024;
                int remainingBytes = 0;
                Console.WriteLine("Receiving File...");
               
                //WHILE RECEIVING FILE FROM CLIENT
                while (bytesReceived < bytesLength)
                {
                    remainingBytes = bytesLength - bytesReceived;
                    if (remainingBytes < size)
                    {
                        size = remainingBytes;
                    }
                    bytesRead = tcpClient.GetStream().Read(buffer, bytesReceived, size);
                    bytesReceived += bytesRead;
                    Console.WriteLine("Bytes Received " + bytesReceived + " bytes");
                }

                //RECEIVE FILES FROM STREAM CONNECTION
                using (FileStream fStream = new FileStream(Path.GetFileName(cmdFileName), FileMode.Create))
                {
                    fStream.Write(buffer, 0, buffer.Length);
                    fStream.Flush();
                    fStream.Close();
                }

                //FILE RECEIVED IN DIRECTORY
                Console.WriteLine("File received and saved in " + Environment.CurrentDirectory);

                //DECRYPT THE COMPRESSED FILE IN DIRECTORY
                DecryptFile(@".\Encrypted File", @".\Decrypted File.zip", magicWord);
                Console.WriteLine("File decrypted in " + Environment.CurrentDirectory + @"\Decrypted File.zip");

                //DECOMPRESS/EXTRACT THE FILE
                ZipFile.ExtractToDirectory(@".\Decrypted File.zip", @".\Extracted File");
                Console.WriteLine("File extracted in " + Environment.CurrentDirectory + @"\Extracted File");

                Console.WriteLine("Press enter to exit");
                Console.ReadKey();
                break;
                
            }
        }
    }
}