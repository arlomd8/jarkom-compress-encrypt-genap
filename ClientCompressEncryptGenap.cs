//CLIENT SIDE

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.IO.Compression;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;


namespace FileTransferClient
{
    class ClientCompressEncryptGenap
    {
        //AES ENCRYPT
        public static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())        
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        //METHOD ENCRYPT FILE
        public static void EncryptFile(string file, string fileEncrypted, string password)
        {
            byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            File.WriteAllBytes(fileEncrypted, bytesEncrypted);
        }

        //MAIN FUNCTION
        static void Main(string[] args)
        {
            try
            {
                //CONNECT TO SERVER
                TcpClient tcpClient = new TcpClient("127.0.0.1", 8080);
                Console.WriteLine("Connected To FTP Server");

                //INSTANTIATE STREAMWRITER
                StreamReader reader = new StreamReader(tcpClient.GetStream());
                StreamWriter writer = new StreamWriter(tcpClient.GetStream());

                //INPUT FOLDER 
                Console.WriteLine("Please enter a full file path");
                string fileName = Console.ReadLine();

                //INPUT PASSWORD
                string magicWord = "AH5X7GSPIC3LL";

                //INITIATE VARIABEL  
                string startPath = @fileName;
                string zipPath = @".\Compressed File.zip";
                string encryptPath = @".\Encrypted File";

                //COMRPRESS THE FILE
                ZipFile.CreateFromDirectory(startPath, zipPath);
                Console.WriteLine("File Successfully Compressed in : " + Environment.CurrentDirectory);

                //ENCRYPT THE COMPRESSED FILE
                EncryptFile(zipPath, encryptPath, magicWord); 
                Console.WriteLine("File Successfully Decrypted in : " + Environment.CurrentDirectory);

                //CONVERT TO BYTES
                byte[] bytes = File.ReadAllBytes(encryptPath);

                //SEND STRING TO SERVER
                writer.WriteLine(magicWord);
                writer.Flush();

                writer.WriteLine(bytes.Length.ToString());
                writer.Flush();

                writer.WriteLine(encryptPath);
                writer.Flush();

                //SEND THE FILE
                Console.WriteLine("Sending the file...");
                tcpClient.Client.SendFile(encryptPath);
                Console.WriteLine("Sent");
                Console.WriteLine("Press enter to exit");
                Console.ReadKey();
                
            }

            
            //THROW EXCEPTION
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
            Console.Read();

        }
    }
}